package com.gmail.v.varvaruk89;

public class Vector3d {
	private double x;
	private double y;
	private double z;

	public Vector3d(double x, double y, double z) {
		super();
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vector3d() {
		super();
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}

	@Override
	public String toString() {
		return "Vector3d [x=" + x + ", y=" + y + ", z=" + z + "]";
	}

	public Vector3d addVector(Vector3d vector) {
		double x = this.x + vector.x;
		double y = this.y + vector.y;
		double z = this.z + vector.z;
		return new Vector3d(x, y, z);
	}

	public double scalarProduct(Vector3d twoVector) {
		double scalarProduct = this.x * twoVector.x + this.y * twoVector.y + this.z * twoVector.z;

		return scalarProduct;
	}

	public Vector3d vectorProduct(Vector3d twoVector) {
		double a = this.y * twoVector.z - this.z * twoVector.y;
		double b = this.z * twoVector.x - this.x * twoVector.z;
		double c = this.x * twoVector.y - this.y * twoVector.x;
		return new Vector3d(a, b, c);

	}

}
