package com.gmail.v.varvaruk89;

public class Main {

	public static void main(String[] args) {
		Vector3d oneVector = new Vector3d(1, 2, 3);
		Vector3d twoVector = new Vector3d(3, 2, 1);
		System.out.println(oneVector);
		System.out.println(twoVector);
		System.out.println(oneVector.addVector(twoVector));
		System.out.println(oneVector.scalarProduct(twoVector));
		System.out.println(oneVector.vectorProduct(twoVector));
	}

}
