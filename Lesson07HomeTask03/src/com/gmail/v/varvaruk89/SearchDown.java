package com.gmail.v.varvaruk89;

import java.io.File;

public class SearchDown implements Runnable {

	private File fileS;
	private File folderTo;
	private Thread thr;

	public SearchDown(File fileS, File folderTo) {
		super();
		this.fileS = fileS;
		this.folderTo = folderTo;
		thr = new Thread(this);
		thr.start();
	}

	@Override
	public void run() {
		File[] array = folderTo.listFiles();
		try {
			for (File file : array) {
				if (file.isDirectory()) {
					Thread thr = new Thread(new SearchDown(fileS, file));
				}

				if (file.getName().equals(fileS.getName())) {
					System.out.println(file.getAbsolutePath());
				}
			}
		} catch (NullPointerException e) {
		}

	}
}
