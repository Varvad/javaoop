package com.gmail.v.varvaruk89;

import java.util.HashMap;


public class Counter {
	private HashMap<Object, Integer> map = new HashMap<>();

	public Counter() {
		super();

	}

	public void repetition(Object[] array) {
		for (int i = 0; i < array.length; i++) {
			Integer count;
			count = map.get(array[i]);
			if (count == null) {
				count = 1;
			} else {
				count++;
			}
			map.put(array[i], count);

		}

		System.out.println(map);
	}

	
}
