package com.gmail.v.varvaruk89;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

public class MySearchEngineWords {
	private File file;
	private String str;

	public MySearchEngineWords() {
		super();
	}

	public void mySearchEngineWords(File fileInOne, File fileInTwo) {

		String lineOne = fileInLines(fileInOne);
		String lineTwo = fileInLines(fileInTwo);
		searchWords(lineOne, lineTwo);
		String words = searchWords(lineOne, lineTwo);
		printInFile(words);

	}

	public String fileInLines(File file) {
		String line = "";

		try (BufferedReader f = new BufferedReader(new FileReader(file))) {
			String str;
			for (; (str = f.readLine()) != null;)
				line = line + str + "\n";
		} catch (IOException e) {
			System.out.println("ERROR");
		}

		return line;
	}

	public String searchWords(String lineOne, String lineTwo) {
		String words = "";
		String[] arrayWordsOne = lineOne.split("[- ,;:.!?\\s]+");
		String[] arrayWordsTwo = lineTwo.split("[- ,;:.!?\\s]+");
		for (String wordOne : arrayWordsOne) {
			for (String wordTwo : arrayWordsTwo) {
				if (wordOne.equalsIgnoreCase(wordTwo)) {
					words += wordOne + " ";
				}
			}
		}

		return words;
	}

	public void printInFile(String words) {
		try (PrintWriter a = new PrintWriter("words.txt")) {
			a.println(words);
			System.out.println("I wrote down everything I could find in the file - words.txt.");

		} catch (FileNotFoundException e) {
			System.out.println("ERROR FILE WRITE");
		}
	}

}
