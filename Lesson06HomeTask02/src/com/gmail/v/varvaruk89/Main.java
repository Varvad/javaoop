package com.gmail.v.varvaruk89;


		import java.util.Random;

		public class Main {

			public static void main(String[] args) {
			
				
				int[] arr = new int[100000000];
				Random rd = new Random();
				
				for (int i = 0; i < arr.length; i++) {
					arr[i] = rd.nextInt(10);
				}
				
				long stWork = System.currentTimeMillis();
				long sum = Sum.sum(arr);
				long endWork = System.currentTimeMillis();
				System.out.println(endWork - stWork +" ms - " + sum + " - Static method");
				
				stWork = System.currentTimeMillis();
				sum = CalcSumMultiThread.calcSum(arr);
				endWork = System.currentTimeMillis();
				System.out.println(endWork - stWork +" ms -" + sum + " - MultiThread method" );
			}

		}
	
	

