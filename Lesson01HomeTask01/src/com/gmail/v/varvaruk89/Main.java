package com.gmail.v.varvaruk89;

public class Main {

	public static void main(String[] args) {
		Cat mura = new Cat("Mura", 5, 7);
		mura.kindness();
		Cat turok = new Cat();
		turok.setName("Turok");
		turok.setOld(3);
		turok.setWeight(6);
		turok.kindness();
		Cat gura = new Cat("Gura", 0, 0);
		Cat intel = new Cat("Intel", 0, 0);

		System.out.println(mura);
		System.out.println(turok);
		System.out.println(gura);
		System.out.println(intel);

		System.out.print("Mura vs Turok = ");
		mura.battle(turok);
		System.out.print("Gura vs Intel = ");
		gura.battle(intel);
		System.out.print("Gura vs Turok = ");
		gura.battle(turok);

	}

}
