package com.gmail.v.varvaruk89;

public class Cat {

	private String name;
	private int old;
	private double weight;
	private int kindness;

	public Cat(String name, int old, double weight) {
		super();
		this.name = name;
		this.old = old;
		this.weight = weight;
	}

	public Cat() {
		super();

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getOld() {
		return old;
	}

	public void setOld(int old) {
		this.old = old;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		return "Cat [name=" + name + ", old=" + old + ", weight=" + weight + ", kindness=" + kindness + "]";
	}

	public void kindness() {
		this.kindness = (int) (weight / old);
	}

	public void battle(Cat cat) {
		if (this.old != 0 | cat.old != 0) {
			if (this.old > cat.old) {
				System.out.println("Winner is " + this.name + ".");
			} else {
				System.out.println("Winner is " + cat.name + ".");
			}
		} else {
			System.out.println("Cats are not born yet.");
		}

	}
}
