package com.gmail.v.varvaruk89;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyFile {
	public void copyMethod(File inFolder, File outFolder) {
		MyFileFilter mFF = new MyFileFilter();
		File[] fileList = inFolder.listFiles(mFF);
		if (fileList.length == 0) {
			System.out.println("Files not found.");
		} else {

			for (File file : fileList) {
				System.out.println(nameOut(file, outFolder));
				copy(file.getAbsolutePath(), nameOut(file, outFolder));
				System.out.println(file);
			}
		}
	}

	public String nameOut(File file, File outFolder) {
		StringBuffer sb = new StringBuffer("");
		sb.append(outFolder);
		sb.append("\\");
		sb.append(file.getName());
		return sb.toString();
	}

	public void copy(String in, String out) {
		try (FileInputStream fis = new FileInputStream(in); FileOutputStream fos = new FileOutputStream(out)) {
			byte[] buffer = new byte[1024];
			int byteread = 0;
			for (; (byteread = fis.read(buffer)) > 0;) {
				fos.write(buffer, 0, byteread);
			}
		} catch (IOException e) {
			System.out.println(e);
		}
	}

}
