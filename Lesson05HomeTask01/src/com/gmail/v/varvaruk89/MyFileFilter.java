package com.gmail.v.varvaruk89;

import java.io.File;
import java.io.FilenameFilter;

public class MyFileFilter implements FilenameFilter {
	File file;
	String str;

	public MyFileFilter(File file, String str) {
		super();
		this.file = file;
		this.str = str;
	}

	
	public MyFileFilter() {
		super();
	}


	@Override
	public boolean accept(File directory, String fileName) {
		if (fileName.endsWith(".txt")) {
			return true;
		}
		return false;
	}

}
