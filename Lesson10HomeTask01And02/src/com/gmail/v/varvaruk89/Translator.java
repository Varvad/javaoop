package com.gmail.v.varvaruk89;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;

public class Translator {

	private HashMap<String, String> dictionary = new HashMap<>();

	public Translator() {
		super();

	}

	public void dictionaryIn(File file) throws IOException {
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String text = " ";
			while ((text = br.readLine()) != null) {
				String[] tmp = text.split("-");
				String key = tmp[0];
				String value = tmp[1];
				dictionary.put(key, value);
			}

		} catch (Exception e) {
			System.out.println("Alarm");

		}

	}

	public void dictionaryInKeyboard(File file) throws IOException {
		String test = "y";
		String saveTest = "y";
		String text;
		InputStream stream = System.in;
		InputStreamReader inputStreamReader = new InputStreamReader(stream);
		BufferedReader reader = new BufferedReader(inputStreamReader);
		while (test.equals("y") | test.equals("Y")) {
			System.out.println("Enter words by example (vocabulary-�������) and press Enter.");
			text = reader.readLine();
			if (text.length() != 0) {
				text = System.lineSeparator() + text.toLowerCase();

				System.out.println("Would you like to save to a file? y/n");
				saveTest = reader.readLine();
				if (saveTest.length() == 0) {
					System.out.println("We are not going to save.");
				}
				if (saveTest.equals("y") | saveTest.equals("Y")) {
					stringToFileAdd(text, file);

				}

				System.out.println("Do you want to add words?y/n");
				test = reader.readLine();

			} else {
				System.out.println("There is nothing to save. I use the dictionary that I have");
				break;
			}
		}

	}

	public void retranslator(File english, File ukrainian) {
		String[] array = fileToString(english).split(" ");
		System.out.println("Translit - " + translit(array));
		stringToFile((translit(array)), ukrainian);
	}

	public String fileToString(File file) {
		StringBuilder sb = new StringBuilder();
		try (BufferedReader bf = new BufferedReader(new FileReader(file))) {
			String readedLine = "";
			while ((readedLine = bf.readLine()) != null) {
				sb.append(readedLine + " ");
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		System.out.println("Read " + file.getName() + " - " + sb.toString());
		return sb.toString().toLowerCase();
	}

	private String translit(String[] array) {
		String str = " ";
		for (int i = 0; i < array.length; i++) {
			if (array[i] == null) {
				str = str + "";
			} else {
				str = str + dictionary.get(array[i]) + " ";
			}
		}

		return str;
	}

	private void stringToFileAdd(String str, File file) {
		try {
			FileWriter writer = new FileWriter(file, true);
			BufferedWriter bufferWriter = new BufferedWriter(writer);
			bufferWriter.write(str);
			System.out.println("I wrote  in " + file.getAbsolutePath());
			bufferWriter.close();
		} catch (IOException e) {
			System.out.println(e);
		}
	}

	private void stringToFile(String str, File file) {
		try (PrintWriter pw = new PrintWriter(file)) {
			pw.println(str);
			System.out.println("I wrote transfer   " + file.getName());
		} catch (FileNotFoundException e) {
			System.out.println("FileNotFoundException");
		}

	}
}