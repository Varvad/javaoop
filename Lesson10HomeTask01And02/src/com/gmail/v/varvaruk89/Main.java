package com.gmail.v.varvaruk89;

import java.io.File;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		Translator tr = new Translator();
		File dictionary = new File("C:/Users/user/workspace/Lesson10HomeTask01And02/dictionary.txt");
		File english = new File("C:/Users/user/workspace/Lesson10HomeTask01And02/English.in");
		File ukrainian = new File("C:/Users/user/workspace/Lesson10HomeTask01And02/Ukrainian.out");
		
		tr.dictionaryInKeyboard(dictionary);
		tr.dictionaryIn(dictionary);
		tr.retranslator(english, ukrainian);

	}

}
