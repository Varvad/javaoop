package com.gmail.v.varvaruk89;

public class LettersAndCounter {
	private char letter;
	private int counter;
	private int textcharsize;
	private double relativeFrequency;
	

	public LettersAndCounter(char letter, int counter, int textcharsize) {
		super();
		this.letter = letter;
		this.counter = counter;
		this.textcharsize = textcharsize;
		stata();
		
	}

	public LettersAndCounter() {
		super();

	}

	public double getRelativeFrequency() {
		return relativeFrequency;
	}

	public char getLetter() {
		return letter;
	}

	public void setLetter(char letter) {
		this.letter = letter;
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public void stata(){
		if (counter!=0){
			relativeFrequency= (double)counter/(double)textcharsize;
			
			
		}else{
			textcharsize=0;
		}
	}

	@Override
	public String toString() {
		return "letter=" + letter + ", counter=" + counter + ", relativeFrequency="
				+ relativeFrequency +System.lineSeparator();
	}
	
	
	
	
	
}
