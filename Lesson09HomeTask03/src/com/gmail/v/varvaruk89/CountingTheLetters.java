package com.gmail.v.varvaruk89;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CountingTheLetters {

	private File file;
	private ArrayList<Character> textInChar = new ArrayList<>();
	private ArrayList<LettersAndCounter> lac = new ArrayList<>();
	private List<Character> alphabet = new ArrayList<>();

	public CountingTheLetters(File file) {
		super();
		this.file = file;

	}

	public CountingTheLetters() {
		super();

	}

	public List<Character> getAlphabet() {
		return alphabet;
	}

	
	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public void starter() {
	try{
		String str = fileToString().toLowerCase();
		for (int i = 0; i < str.length(); i++) {
			textInChar.add(str.charAt(i));

		}
		alphabetIn();
		mycomparing();
		sortedByCounter();
		System.out.println(toString());
	}catch(NullPointerException e){
		System.out.println("Krasava a gde dannue?");
	}
	}

	public String fileToString() {
		StringBuilder sb = new StringBuilder();
		try (BufferedReader bf = new BufferedReader(new FileReader(file))) {
			String readedLine = "";
			while ((readedLine = bf.readLine()) != null) {
				sb.append(readedLine + " ");
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return sb.toString();
	}

	public void alphabetIn() {
		char[] alphabetChar = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		for (char c : alphabetChar) {
			alphabet.add(c);
		}
	}

	public void mycomparing() {
		for (Character character : alphabet) {
			int counter = 0;
			for (Character characterTwo : textInChar) {
				if (character.equals(characterTwo)) {
					counter++;
				}

			}
			lac.add(new LettersAndCounter(character,counter,textInChar.size()));
		}
	}
	public void sortedByCounter(){
		lac.sort(new SortedByRelativeFrequency());
	}

	@Override
	public String toString() {
		
	String str="";
	for (LettersAndCounter element : lac) {
		str = str +element;
	}
	
	
	return str;
	
	
	}


}
