package com.gmail.v.varvaruk89;

import java.util.Comparator;

public class SortedByRelativeFrequency implements Comparator<LettersAndCounter> {

	@Override
	public int compare(LettersAndCounter one, LettersAndCounter two) {
		
		double counter = 0;
		double counterTwo = 0;
		try {
			counter = one.getRelativeFrequency();
			counterTwo = two.getRelativeFrequency();

		} catch (NullPointerException e) {

		}

		if (counter > counterTwo) {
			return -1;
		} else if (counter < counterTwo) {
			return 1;
		} else {
			return 0;
		}
	}

	
}
