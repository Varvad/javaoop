package gmail.com.v.varvaruk89;

import java.util.Arrays;
import java.util.Random;

public class Main {

	public static void main(String[] args) {
		int[] arr = new int[100000];
		Random rd = new Random();

		for (int i = 0; i < arr.length; i++) {
			arr[i] = rd.nextInt(10);
		}

		int[] array2 = arr.clone();
		
		
		long stWork = System.currentTimeMillis();
		Arrays.toString(Sorting.sorting(arr));
		long endWork = System.currentTimeMillis();
		System.out.println(endWork - stWork + " ms - " + " - Static method");

		stWork = System.currentTimeMillis();
		MultiThreadSorting.sort(array2,4);
		endWork = System.currentTimeMillis();
		System.out.println(endWork - stWork + " ms -" + " - MultiThread method");
		
//		System.out.println(Arrays.toString(arr));
//		System.out.println(Arrays.toString(array2));
		
		
					
			
		
	}
}
