package gmail.com.v.varvaruk89;

public class Sorting {
public static int [] sorting (int...array){
	int step = array.length/2;
	while(step>0){
		for(int i =0;i<(array.length-step); i++){
			int j =i;
			while (j >= 0 && array[j] > array[j + step]){
				int temp = array[j];
				array[j] = array[j + step];
				array[j + step] = temp;
                j--; 
            }
			}
		step = step / 2;
	
	}
			return array;
}
}
