package com.gmail.v.varvaruk89;

import java.util.Arrays;
import javax.swing.JOptionPane;

public class Group implements Voenkom {
	private String nameGroup;
	private Student[] group = new Student[10];
	private int countStudent;

	public Group() {
		super();
	}

	public String getNameGroup() {
		return nameGroup;
	}

	public void setNameGroup(String nameGroup) {
		this.nameGroup = nameGroup;
	}

	public int getCountStudent() {
		return countStudent;
	}

	public void setCountStudent(int countStudent) {
		this.countStudent = countStudent;
	}

	public void addStudent(Student s) {
		try {
			if (countStudent == 10) {
				throw new OverflowGroupException();

			} else {
				for (int i = 0; i < group.length; i++) {
					if (group[i] == null) {
						group[i] = s;
						s.setGroup(nameGroup);
						countStudent++;
						System.out.println(nameGroup + " added " + s.getName());
						break;
					}

				}
			}

		} catch (OverflowGroupException e) {
			System.out.println(e.getMessage());
		}

	}

	public void addStudentFromTheKeyboard() {
		while (true) {

			try {
				boolean sex = false;
				String name = String.valueOf(JOptionPane.showInputDialog("Enter  name."));

				if (name.equals("")) {

					throw new NullPointerException();

				}

				String lastName = String.valueOf(JOptionPane.showInputDialog("Enter last name"));
				if (lastName.equals("")) {

					throw new NullPointerException();

				}
				int age = Integer.valueOf(JOptionPane.showInputDialog(" Enter age"));
				String sexString = String.valueOf(JOptionPane.showInputDialog("Enter  sex - format M or W"));
				if (!(sexString.equals("M") | sexString.equals("W") | sexString.equals("m") | sexString.equals("w"))) {
					throw new NumberFormatException();
				} else {
					if (sexString.equals("M") | sexString.equals("m")) {
						sex = true;
					}
				}

				double averageRating = Double.valueOf(JOptionPane.showInputDialog("Enter averageRating"));
				String faculty = String.valueOf(JOptionPane.showInputDialog("Enter faculty"));
				if (lastName.equals("")) {

					throw new NullPointerException();

				}
				addStudent(new Student(name, lastName, age, sex, averageRating, faculty, this.nameGroup));

			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(null, "Parameter not entered,Format Exception!");
				break;

			} catch (NullPointerException e) {
				JOptionPane.showMessageDialog(null, "Parameter not entered,NullPointerException!");
				break;
			}

		}
	}

	public void delStudent(Student s) {
		try {
			if (countStudent == 0) {
				throw new NullGroupException();
			} else {

				for (int i = 0; i < group.length; i++) {
					if (group[i].equals(s)) {

						s.setGroup("Spirit");
						System.out.println();
						System.out.print("Deleted ");
						group[i].outInfo();
						group[i] = null;
						countStudent = countStudent - 1;
						break;
					}
				}
			}
		} catch (NullGroupException e) {
			System.out.println(e.getMessage());
		}
	}

	public void printInfoGroup() {

		if (countStudent == 0) {
			System.out.println("The group is empty");
		} else {
			System.out.println();
			System.out.println("Name group " + nameGroup + " number of students " + countStudent + ":");
			for (int i = 0; i < countStudent; i++) {
				if (group[i] != null) {
					group[i].outInfo();
				}
			}
		}
	}

	public void findStudentByLastName(String lastName) {
		boolean test = false;
		for (int i = 0; i < countStudent; i++) {
			if (group[i].getLastName().equals(lastName)) {
				group[i].outInfo();
				test = true;
			}
		}
		if (!test) {
			System.out.println("I can not find a student");
		}
	}

	@Override
	public String toString() {
		try {
			Arrays.sort(group);
		} catch (NullPointerException e) {

		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < group.length; i++) {
			if (group[i] == null) {
				continue;
			} else {
				sb.append("\n" + group[i]);
			}
		}
		return sb.toString();
	}

	public void sortedByName() {
		Arrays.sort(group, new SortedByName());
	}

	public void sortedByLastName() {
		Arrays.sort(group, new SortedByLastName());
	}

	public void sortedByAge() {
		Arrays.sort(group, new SortedByAge());
	}

	public void sortedBySex() {
		Arrays.sort(group, new SortedBySex());
	}

	public void sortedByAverageRating() {
		Arrays.sort(group, new SortedByAverageRating());
	}

	@Override
	public Student[] soldiers() {
		int counter=0;
		Student [] soldiers = new Student[counter]; 
	
	try{
		for(int i =0;i<group.length;i++){
		if(group[i].isSex()&group[i].getAge()>18){
			counter++;
			Student [] bufer = new Student[counter];
			System.arraycopy(soldiers, 0, bufer, 0, soldiers.length);
			soldiers = bufer;
			soldiers[counter-1]=group[i]; 
			
		}
			
		}
	}catch(NullPointerException e){
		
	}
			
		return soldiers;
	}

}
