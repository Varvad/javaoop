package com.gmail.v.varvaruk89;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

public class FileMonster {

	public FileMonster() {
		super();

	}

	public void infoOutCsv(Group group) {
		String str = group.toString();
		String[] array = str.split(" ");
		int count = group.getCountStudent();
		System.out.println(Arrays.toString(array));
		System.out.println(count);

		try (PrintWriter pw = new PrintWriter(new File("Group.csv"))) {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < array.length; i++) {
				sb.append(array[i]);
				if (i < array.length - 1) {
					sb.append(';');
				}
			}
			pw.write(sb.toString());
			pw.close();
			System.out.println("done in Group.csv!");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void ˝reateObjectinFile(File fileName) {

		String nameGroup = fileName.getName();
		int i = nameGroup.lastIndexOf('.');
		if (i != -1) {
			nameGroup = nameGroup.substring(0, i);
		}
		Group test = new Group();
		test.setNameGroup(nameGroup);
		System.out.println("Created group " + nameGroup + ".");
		String[] studentArray = new String[8];
		try (BufferedReader bf = new BufferedReader(new FileReader(fileName))) {
			String str = "";
			for (; (str = bf.readLine()) != null;) {
				String[] array = str.split(";");
				fromFileToObject(array, test);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		test.printInfoGroup();

	}

	public void fromFileToObject(String[] studentArray, Group group) {
		String name = "";
		String lastName = "";
		int age = 0;
		double averageRating = 0;
		boolean sex = false;
		String faculty = "";
		try {
			name = studentArray[0];
			lastName = studentArray[1];
			age = Integer.parseInt(studentArray[2]);
			if (studentArray[3].equals("M") | studentArray[3].equals("m")) {
				sex = true;
			}
			faculty = studentArray[4];
			averageRating = Double.parseDouble(studentArray[8]);

			group.addStudent(new Student(name, lastName, age, sex, averageRating, faculty, group.getNameGroup()));
		} catch (NullPointerException e) {
		} catch (ArrayIndexOutOfBoundsException e2) {

		}
	}
}
