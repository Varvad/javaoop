package com.gmail.v.varvaruk89;

public class NullGroupException extends Exception {

	private static final long serialVersionUID = 1L;

	@Override
	public String getMessage() {
		return "There are no students in the group";

	}

}
