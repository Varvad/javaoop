package com.gmail.v.varvaruk89;

import java.util.Comparator;

public class SortedBySex implements Comparator<Student> {

	@Override
	public int compare(Student studentOne, Student studentTwo) {

		boolean sexOne = false;
		boolean sexTwo = false;

		try {
			sexOne = studentOne.isSex();
			sexTwo = studentTwo.isSex();

		} catch (NullPointerException e) {

		}

		if (sexOne & !sexTwo) {
			return 1;
		}

		if(sexTwo&!sexOne){
			return -1;
			
		}else{
			return 0;
		}
	}

}
