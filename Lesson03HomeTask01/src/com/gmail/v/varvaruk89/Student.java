package com.gmail.v.varvaruk89;

public class Student extends Human implements Comparable {
	private double averageRating;
	private String faculty;
	private String group = "Spirit";

	public Student(String name, String lastName, int age, boolean sex, double averageRating, String faculty,
			String group) {
		super(name, lastName, age, sex);
		this.averageRating = averageRating;
		this.faculty = faculty;
		this.group = group;
	}

	public Student() {
		super();
	}

	public Student(String name, String lastName, int age, boolean sex) {
		super(name, lastName, age, sex);
	}

	
	public double getAverageRating() {
		return averageRating;
	}

	public void setAverageRating(double averageRating) {
		this.averageRating = averageRating;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public int compareTo(Object obj)
	{		
		Student tmp = (Student)obj;
		if(this.getName().charAt(0)<tmp.getName().charAt(0)){
			return -1;
		}else if (this.getName().charAt(0)>tmp.getName().charAt(0)){
			return 1;
					
		}
		
	return 0;	
	}
	
	
	
	@Override
	public void outInfo() {
		System.out.println(this.getName() + " " + this.getLastName() + " " + this.getAge() + " "
				+ (this.isSex() == true ? "M" : "W") + " " + faculty +" "+ group+" Average rating " + averageRating);
	}

	@Override
	public String toString() {
	String 	s = (this.getName() + " " + this.getLastName() + " " + this.getAge() + " "
				+ (this.isSex() == true ? "M" : "W") + " " + faculty +" "+ group+" Average rating " + averageRating);
		return s;
	}

}
