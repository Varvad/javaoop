package com.gmail.v.varvaruk89;

import java.io.Serializable;

public class Student extends Human implements Serializable {

	private static final long serialVersionUID = 1L;

	private double averageRating;
	private String faculty;
	private String group = "Spirit";

	public Student(String name, int age, char sex, double averageRating, String faculty, String group) {
		super(name, age, sex);
		this.averageRating = averageRating;
		this.faculty = faculty;
		this.group = group;
	}

	public Student() {
		super();

	}

	public double getAverageRating() {
		return averageRating;
	}

	public void setAverageRating(double averageRating) {
		this.averageRating = averageRating;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	@Override
	public String toString() {
		return "Student " + super.toString() + " " + "averageRating=" + averageRating + ", faculty=" + faculty
				+ ", group=" + group + ".";
	}

}
