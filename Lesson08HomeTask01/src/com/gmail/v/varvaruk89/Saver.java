package com.gmail.v.varvaruk89;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Saver {

	public static void outputStream(Group group, String fileName) {
		try (ObjectOutputStream OOS = new ObjectOutputStream(new FileOutputStream(fileName))) {
			OOS.writeObject(group);
		} catch (IOException e) {
			System.out.println("Error save!");
		}
	}

	public static Group inputStream(String nameGroup, String fileName) throws ClassNotFoundException {
		Group tmp = new Group(nameGroup);
		try (ObjectInputStream OIS = new ObjectInputStream(new FileInputStream(fileName))) {
			tmp = (Group) OIS.readObject();

		} catch (IOException e) {
			System.out.println("ERROR load group !!!");

		}

		return tmp;

	}
}
