package com.gmail.v.varvaruk89;

import java.io.Serializable;

public class Group implements Serializable {

	private static final long serialVersionUID = 1L;
	private Student[] group = new Student[0];
	private String nameGroup;
	private int countStudent = 0;

	public Group(String nameGroup) {
		super();
		this.nameGroup = nameGroup;
	}

	public Group() {
		super();

	}

	public void addStudent(Student student) {
		Student[] c = new Student[group.length + 1];
		System.arraycopy(group, 0, c, 0, group.length);
		student.setGroup(nameGroup);
		countStudent = countStudent + 1;
		c[c.length - 1] = student;
		group = c;

	}

	@Override
	public String toString() {
		String groupString = "";

		if (countStudent == 0) {
			groupString = groupString + "Group is empty";
		} else {
			System.out.println();
			System.out.println("Name group " + nameGroup + " number of students " + countStudent + ": ");
			for (int i = 0; i < this.group.length; i++) {
				if (group[i] != null) {
					groupString = groupString + " " + group[i].toString() + System.lineSeparator();

				}
			}

		}

		return groupString;

	}

}
