package com.gmail.v.varvaruk89;

import java.io.Serializable;

public class Human implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	private int age;
	private char sex;

	public Human(String name, int age, char sex) {
		super();
		this.name = name;
		this.age = age;
		this.sex = sex;
	}

	public Human() {
		super();
	}

	@Override
	public String toString() {
		return "name=" + name + ", age=" + age + ", sex=" + sex;
	}

}
