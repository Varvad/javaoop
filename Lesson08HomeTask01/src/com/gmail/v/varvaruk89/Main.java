package com.gmail.v.varvaruk89;

public class Main {

	public static void main(String[] args) throws ClassNotFoundException {

		Student st = new Student("Tolya", 21, 'M', 89, "IT", "Groupe");
		Student qt = new Student("Edik", 22, 'M', 89, "IT", "Groupe");
		Student wt = new Student("Dima", 22, 'M', 89, "IT", "Groupe");
		Student et = new Student("Vadim", 22, 'M', 89, "IT", "Groupe");
		Student rt = new Student("Genya", 22, 'M', 89, "IT", "Groupe");

		System.out.println(st);
		Group dum = new Group("Dum");
		System.out.println(dum);
		dum.addStudent(st);
		dum.addStudent(qt);
		dum.addStudent(wt);
		dum.addStudent(et);
		dum.addStudent(rt);

		System.out.println(dum);
		Saver.outputStream(dum, "test.txt");

		System.out.println(Saver.inputStream("test", "test.txt"));

	}

}
