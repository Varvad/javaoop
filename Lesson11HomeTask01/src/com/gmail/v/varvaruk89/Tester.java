package com.gmail.v.varvaruk89;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Tester {

	public static void testAdressFromFile(File file) {
		HashSet<String> adressList = fileToString(file);
		for (String string : adressList) {

			try {
				URL url = new URL(string);
				HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
				System.out.println(string + " is " + urlc.getResponseMessage());

			} catch (IOException e) {
				System.out.println(string + "dead is not OK");
			}
		}
	}

	private static HashSet<String> fileToString(File file) {

		HashSet<String> adressList = new HashSet<String>();
		try (BufferedReader bf = new BufferedReader(new FileReader(file))) {
			String readedLine = "";
			while ((readedLine = bf.readLine()) != null) {

				adressList.add(readedLine);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		return adressList;
	}

}
