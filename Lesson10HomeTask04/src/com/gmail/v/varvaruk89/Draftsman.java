package com.gmail.v.varvaruk89;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Draftsman {
	private HashMap<Character, String> dictionary = new HashMap<>();
	private String[] result = new String[20];

	public Draftsman() {
		super();
	}

	public void dictionaryIn(File file) throws IOException {
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String text = " ";
			String textMap = "";
			Character key;

			for (; (text = br.readLine()) != null;) {

				textMap = textMap + text + System.lineSeparator();
				if (text.endsWith("%")) {
					String tmpString[] = textMap.split("-");
					char[] tmp = text.toCharArray();
					key = tmp[tmp.length - 2];
					textMap = textMap + tmpString[0];
					dictionary.put(key, tmpString[0]);
					textMap = "";
				}
			}
		
		} catch (Exception e) {
		}
	}

	public void toArt(String txt) {
		String art = "";
		char[] array = txt.toCharArray();
		
		for (int i = 0; i < array.length; i++) {
			if(	dictionary.get(array[i]) != null){
			toArtFull(dictionary.get(array[i]));
		}
		}
		printresult(result);

	}

	private void toArtFull(String art) {
		String[] array = art.split("\n");
		int count = result.length - 1;
		
		for (int i = array.length - 1; i >= 0; i = i - 1) {
			if (result[count] == null) {
				result[count] = array[i].toString();
			} else {
				result[count] = (result[count]).replaceAll("\\s", "") + array[i] + ("\n");
				
			}
			count = count - 1;
		}
		for(int i =result.length-array.length-1;i>=0;i=i-1){
			if (result[i] == null) {
				result[i] = "__________________________"+"\n";
			}else{
				result[i]=result[i].replaceAll("\\s", "")+"__________________________"+"\n";
			}
		}
		
		
	}

	private static void printresult(String[] result) {
		String txt = "";
		for (int i = 0; i < result.length; i++) {
			if (!(result[i] == null)) {
				txt = txt + result[i];
			}
		}
		System.out.println(txt);
	}

}