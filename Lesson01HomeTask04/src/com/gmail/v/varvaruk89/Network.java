package com.gmail.v.varvaruk89;

public class Network {
	private static int counter = 1;
	private static String[] baseNetwork = new String[100];

	public static String[] getArray() {
		return baseNetwork;
	}

	public static void addToNetwork(String phoneNumber) {
		if (counter == baseNetwork.length - 1) {
			counter = 0;
		}
		baseNetwork[counter++] = phoneNumber;

	}
}