package com.gmail.v.varvaruk89;

public class Phone {

	private String phoneNumber;

	public Phone(String phoneNumber) {
		super();
		this.phoneNumber = phoneNumber;
	}

	public Phone() {
		super();
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "Phone [phoneNumber=" + phoneNumber + "]";
	}

	public void checkIn() {
		Network.addToNetwork(phoneNumber);
	}

	public Boolean testNetwork(String phoneNumber) {
		String[] baseNetwork = Network.getArray();
		Boolean test = false;
		for (int i = 0; i < baseNetwork.length; i++) {
			if (phoneNumber.equals(baseNetwork[i])) {
				test = true;
			}
		}
		return test;
	}

	public void call(Phone twoPhone) {
		
		if (testNetwork(this.phoneNumber)) {
			if (testNetwork(twoPhone.phoneNumber)) {
				for (int i = 0; i < 3; i++) {
					System.out.println("Ring the bell.");
				}

			} else {
				System.out.println("This phone " + twoPhone.phoneNumber + " is not registered in the base network.");

			}

		} else {
			System.out.println("This phone " + this.phoneNumber + " is not registered in the base network.");
		}
	}

}
