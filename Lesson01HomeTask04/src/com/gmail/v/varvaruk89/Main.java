package com.gmail.v.varvaruk89;

public class Main {
	public static void main(String[] args) {
		Phone onePhone = new Phone("0501245872");
		onePhone.checkIn();
		Phone twoPhone = new Phone ("0501234567");
		twoPhone.checkIn();
		Phone threePhone = new Phone("0445214264");
		onePhone.call(threePhone);
		twoPhone.call(threePhone);
		onePhone.call(twoPhone);
		twoPhone.call(onePhone);
	}
}
