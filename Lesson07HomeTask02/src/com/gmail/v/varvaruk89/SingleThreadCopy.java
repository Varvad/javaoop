package com.gmail.v.varvaruk89;

import java.io.File;
import java.io.RandomAccessFile;
import java.util.Arrays;

public class SingleThreadCopy implements Runnable {

	private File fileFrom;
	private File fileTo;
	private long start;
	private long end;
	private Thread thr;
	
	
	
	
	public SingleThreadCopy(File fileFrom, File fileTo, long start, long end) {
		super();
		this.fileFrom = fileFrom;
		this.fileTo = fileTo;
		this.start = start;
		this.end = end;
		thr = new Thread(this);
		thr.start();
		
	}

	public SingleThreadCopy() {
		super();

	}


	@Override
	public void run() {
		try (RandomAccessFile in = new RandomAccessFile(fileFrom, "r");
				RandomAccessFile out = new RandomAccessFile(fileTo, "rw");) {

			in.seek(start);
			out.seek(start);
			long i = start;
			
			while (i < end) {
				out.write(in.read());
				i++;
						
						
			}
//			System.out.println( Thread.currentThread().getName());
			
			
		} catch (Exception e) {

		}

	}

}
