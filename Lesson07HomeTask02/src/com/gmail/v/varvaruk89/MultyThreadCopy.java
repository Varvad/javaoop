package com.gmail.v.varvaruk89;

import java.io.File;

public class MultyThreadCopy {
	public static void copyFile(File fileFrom, File fileTo, int cntThread) {
		SingleThreadCopy[] threadarray = new SingleThreadCopy[cntThread];
		boolean test = false;
		if (((long) fileTo.length()) == (long) fileFrom.length()) {
			test = true;

		}

		if (!test) {
			Stata st = new Stata("Procent", fileFrom, fileTo);
			for (int i = 0; i < threadarray.length; i++) {

				long size = (long) fileFrom.length() / cntThread;
				long begin = size * i;
				long end = ((i + 1) * size);
				if (((long) fileFrom.length() - end) < size) {
					end = (long) fileFrom.length();
				}
				threadarray[i] = new SingleThreadCopy(fileFrom, fileTo, begin, end);
			}

		} else {
			System.out.println("No need to copy files the same");
		}

	}

}
