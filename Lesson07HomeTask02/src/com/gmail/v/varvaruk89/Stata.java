package com.gmail.v.varvaruk89;

import java.io.File;

public class Stata implements Runnable {
	private String name;
	private Thread thr;
	private File fileFrom;
	private File fileTo;

	public Stata(String name, File fileFrom, File fileTo) {
		super();
		this.name = name;
		this.fileFrom = fileFrom;
		this.fileTo = fileTo;
		thr = new Thread(this);
		thr.start();
	}

	public Stata() {
		super();

	}

	public void sum() {

		while ((long) fileFrom.length() > (long) fileTo.length()) {
			if (((long) fileTo.length()) == 0) {
				System.out.println("0");
			} else {
				long x = ((((long) fileTo.length()) * 100)) / (long) fileFrom.length();
				System.out.println(x + " %");

			}
		}

	}

	@Override
	public void run() {

		sum();
	}

}
