package com.gmail.v.varvaruk;

import java.util.Comparator;

public class SortedByLastName implements Comparator<Student> {
	
	@Override
	public int compare(Student studentOne, Student studentTwo) {
		String lastNameOne ="";
		String lastNameTwo="";
		try{
			lastNameOne = studentOne.getLastName();
			lastNameTwo = studentTwo.getLastName();
			
		}catch (NullPointerException e) {
			
		}
		
		return lastNameOne.compareTo(lastNameTwo) ;
	}



}
