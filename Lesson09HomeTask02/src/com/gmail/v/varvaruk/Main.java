package com.gmail.v.varvaruk;

import java.io.File;
import java.util.Arrays;

public class Main {

	public static void main(String[] args) {

		Group scoop = new Group();
		scoop.setNameGroup("Scoop");
		 scoop.addStudentFromTheKeyboard();

		Student petya = new Student("Petya", "Aupkin", 15, true, 68, "Transtologi", "Spirit");
		Student vasya = new Student("Vasya", "Bupkin", 16, true, 65, "Transtologi", "Spirit");
		Student kolya = new Student("Kolya", "Cupkin", 17, true, 64, "Transtologi", "Spirit");
		Student gora = new Student("Gora", "Dupkin", 18, true, 69, "Transtologi", "Spirit");
		Student dima = new Student("Dima", "Fupkin", 19, true, 69, "Transtologi", "Spirit");
		Student tina = new Student("Tina", "Gupkina", 29, false, 61, "Transtologi", "Spirit");
		Student inna = new Student("Inna", "Hupkina", 39, false, 62, "Transtologi", "Spirit");
		Student olya = new Student("Olya", "Nupkina", 49, false, 65, "Transtologi", "Spirit");
		Student sergey = new Student("Sergey", "Supkin", 20, true, 68, "Transtologi", "Spirit");
		Student rostik = new Student("Rostik", "Wupkin", 69, true, 65, "Transtologi", "Spirit");
		
		scoop.setNameGroup("Scoop");
		scoop.addStudent(rostik);
		scoop.addStudent(sergey);
		scoop.addStudent(vasya);
		scoop.addStudent(olya);
		scoop.addStudent(inna);
		scoop.addStudent(tina);
		scoop.addStudent(dima);
		scoop.addStudent(gora);
		scoop.addStudent(kolya);
		scoop.addStudent(petya);
		
		System.out.println("+");
		
		System.out.println(scoop);
		
		scoop.sortedByName();
		
		System.out.println(scoop);
		 System.out.println();
		 scoop.printInfoGroup();
		
		 scoop.sortedByLastName();
		 System.out.println();
		 scoop.printInfoGroup();
		
		 scoop.sortedByAge();
		 System.out.println();
		 scoop.printInfoGroup();
		
		 scoop.sortedBySex();
		 System.out.println();
		 scoop.printInfoGroup();
		
		 scoop.sortedByAverageRating();
		 System.out.println();
		 scoop.printInfoGroup();
		 System.out.println(Arrays.toString(scoop.soldiers()));
		 
		
		FileMonsterForJson fMFJ = new FileMonsterForJson();
		fMFJ.infoOutToJson(scoop);
		File test = new File("Scoop.json");
		fMFJ.createObjectInFile(test);

	}

}
