package com.gmail.v.varvaruk;

public class OverflowGroupException extends Exception {

	private static final long serialVersionUID = 1L;

	@Override
	public String getMessage() {
		return "Sorry group is full";

	}
}
