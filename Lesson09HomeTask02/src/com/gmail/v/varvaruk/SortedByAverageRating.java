package com.gmail.v.varvaruk;

import java.util.Comparator;

public class SortedByAverageRating implements Comparator<Student> {

	@Override
	public int compare(Student studentOne, Student studentTwo) {
		double averageRatingOne = 0;
		double averageRatingTwo = 0;
		try {
			averageRatingOne = studentOne.getAverageRating();
			averageRatingTwo = studentTwo.getAverageRating();

		} catch (NullPointerException e) {

		}

		if (averageRatingOne > averageRatingTwo) {
			return 1;
		} else if (averageRatingOne < averageRatingTwo) {
			return -1;
		} else {
			return 0;
		}

	}
}
