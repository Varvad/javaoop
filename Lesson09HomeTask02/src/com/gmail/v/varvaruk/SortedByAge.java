package com.gmail.v.varvaruk;

import java.util.Comparator;

public class SortedByAge implements Comparator<Student> {

	@Override
	public int compare(Student studentOne, Student studentTwo) {
		int ageOne = 0;
		int ageTwo = 0;
		try {
			ageOne = studentOne.getAge();
			ageTwo = studentTwo.getAge();

		} catch (NullPointerException e) {

		}

		if (ageOne > ageTwo) {
			return 1;
		} else if (ageOne < ageTwo) {
			return -1;
		} else {
			return 0;
		}

	}
}