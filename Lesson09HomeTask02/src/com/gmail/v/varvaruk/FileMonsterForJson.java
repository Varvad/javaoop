package com.gmail.v.varvaruk;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;

public class FileMonsterForJson {

	public FileMonsterForJson() {
		super();
	
	}

	public void infoOutToJson(Group group) {
		
		Gson gson = new Gson ();
		String groupJson  = gson.toJson(group);
		
		try(FileWriter fW= new FileWriter(group.getNameGroup()+".json")){
			fW.write(groupJson);
			System.out.println("I wrote in "+group.getNameGroup()+".json");
		}catch(IOException e){
			System.out.println("Problem with the file "+group.getNameGroup()+".json");
		}
		
		
	}
	
	
	public void createObjectInFile(File fileJson) {
		Gson gson = new Gson();
		try(BufferedReader reader = new BufferedReader(new FileReader(fileJson))){
			Group group  = gson.fromJson(reader, Group.class);
			group.printInfoGroup();
			
		}catch (FileNotFoundException e) {
			System.out.println("File not found.");
		}catch(IOException e){
			System.out.println("Alarm IOException");
			
		}
	}

	
	
}
