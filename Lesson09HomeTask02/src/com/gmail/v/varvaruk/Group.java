package com.gmail.v.varvaruk;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Group implements Voenkom {
	private String nameGroup;
	private int countStudent;
	private ArrayList<Student> students = new ArrayList<>();

	public Group() {
		super();
	}

	public String getNameGroup() {
		return nameGroup;
	}

	public void setNameGroup(String nameGroup) {
		this.nameGroup = nameGroup;
	}

	public int getCountStudent() {
		return countStudent;
	}

	public void setCountStudent(int countStudent) {
		this.countStudent = countStudent;
	}

	public void addStudent(Student s) {
		try {
			if (countStudent == 10) {
				throw new OverflowGroupException();

			} else {
				students.add(s);
				s.setGroup(nameGroup);
				countStudent++;
				System.out.println(nameGroup + " added " + s.getName());
			}

		} catch (OverflowGroupException e) {
			System.out.println(e.getMessage());
		}

	}

	public void addStudentFromTheKeyboard() {
		while (true) {

			try {
				boolean sex = false;
				String name = String.valueOf(JOptionPane.showInputDialog("Enter  name."));

				if (name.equals("")) {

					throw new NullPointerException();

				}

				String lastName = String.valueOf(JOptionPane.showInputDialog("Enter last name"));
				if (lastName.equals("")) {

					throw new NullPointerException();

				}
				int age = Integer.valueOf(JOptionPane.showInputDialog(" Enter age"));
				String sexString = String.valueOf(JOptionPane.showInputDialog("Enter  sex - format M or W"));
				if (!(sexString.equals("M") | sexString.equals("W") | sexString.equals("m") | sexString.equals("w"))) {
					throw new NumberFormatException();
				} else {
					if (sexString.equals("M") | sexString.equals("m")) {
						sex = true;
					}
				}

				double averageRating = Double.valueOf(JOptionPane.showInputDialog("Enter averageRating"));
				String faculty = String.valueOf(JOptionPane.showInputDialog("Enter faculty"));
				if (lastName.equals("")) {

					throw new NullPointerException();

				}
				addStudent(new Student(name, lastName, age, sex, averageRating, faculty, this.nameGroup));

			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(null, "Parameter not entered,Format Exception!");
				break;

			} catch (NullPointerException e) {
				JOptionPane.showMessageDialog(null, "Parameter not entered,NullPointerException!");
				break;
			}

		}
	}

	public void delStudent(Student s) {
		try {
			if (countStudent == 0) {
				throw new NullGroupException();
			} else {

				students.remove(s);
				s.setGroup("Spirit");
				System.out.println();
				System.out.print("Deleted ");
				countStudent = countStudent - 1;

			}

		} catch (NullGroupException e) {
			System.out.println(e.getMessage());
		}
	}

	public void printInfoGroup() {

		if (countStudent == 0) {
			System.out.println("The group is empty");
		} else {
			System.out.println();
			System.out.println(toString());

		}
	}

	public void findStudentByLastName(String lastName) {
		boolean test = false;
		for (Student student : students) {
			if (student.getLastName().equals(lastName)) {
				System.out.println(student);
				test = true;
			}

			if (!test) {
				System.out.println("I can not find a student");
			}
		}
	}

	public void sortedByName() {

		students.sort(new SortedByName());
		
	}

	 public void sortedByLastName() {
		 students.sort(new SortedByLastName());
	 }
	
	 public void sortedByAge() {
		 students.sort(new SortedByAge());
	 }
	
	 public void sortedBySex() {
		 students.sort(new SortedBySex());
	 }
	
	 public void sortedByAverageRating() {
		 students.sort(new SortedByAverageRating());
	 }
	

	
	
	@Override
	public Student[] soldiers() {
		int counter = 0;
		Student[] soldiers = new Student[counter];

		try {
			for (Student student : students) {
				if (student.isSex() & student.getAge() > 18) {
					counter++;
					Student[] bufer = new Student[counter];
					System.arraycopy(soldiers, 0, bufer, 0, soldiers.length);
					soldiers = bufer;
					soldiers[counter - 1] = student;

				}

			}
		} catch (NullPointerException e) {

		}

		return soldiers;
	}

	@Override
	public String toString() {
		String str = "nameGroup=" + nameGroup + ", countStudent=" + countStudent+ System.lineSeparator();
		for (Student student : students) {
			str = str +student.toString()+System.lineSeparator();
			
		}
		
		return str;
	}

}
