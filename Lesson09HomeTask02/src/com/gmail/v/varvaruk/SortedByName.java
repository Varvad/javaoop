package com.gmail.v.varvaruk;

import java.util.Comparator;

public final class SortedByName implements Comparator<Student> {
	
	@Override
	public int compare(Student studentOne, Student studentTwo) {
		String nameOne ="";
		String nameTwo="";
		try{
		nameOne = studentOne.getName();
		nameTwo = studentTwo.getName();
			
		}catch (NullPointerException e) {
			
		}
		
		return nameOne.compareTo(nameTwo) ;
	}



}
