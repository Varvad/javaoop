package com.gmail.v.varvaruk89;

import java.math.BigInteger;

public class ThreadsFactorial implements Runnable {
	private  int threadNum;
	
		
	public ThreadsFactorial(int threadNum) {
		super();
		this.threadNum = threadNum;
	}

	public ThreadsFactorial() {
		super();
	}

	 public static BigInteger factorial(int n)
	    {
	        BigInteger ret = BigInteger.ONE;
	        for (int i = 1; i <= n; ++i){
	        	ret = ret.multiply(BigInteger.valueOf(i));
	        }
	        return ret;
	    }

	@Override
	public void run() {
		
		System.out.println("Thread number " + (threadNum) + " factorial is " + factorial(threadNum));
		
	}
	


}
