package com.gmail.v.varvaruk89;

public class Board {
	private Shape[] sectors = new Shape[4];

	public Board() {
		super();
	}

	public void shapeInBoard(Shape figure, int i) {
		if (this.sectors[i - 1] != null) {
			System.out.println("Sector is occupied by another figure.");
		} else {
			this.sectors[i - 1] = figure;
			System.out.println("Sector " + i + " is occupied by " + figure.getClass().getName().split("[.]")[4]);
		}
	}

	public void shapeOutBoard(int i) {
		if (this.sectors[i - 1] != null) {
			this.sectors[i - 1] = null;
			System.out.println("The figure from the " + i + " sector was deleted.");
		} else {
			System.out.println("The sector was empty.");
		}
	}

	public void outInformation() {
		for (int i = 0; i < sectors.length; i++) {
			if (sectors[i] != null) {
				System.out.println("Sectors" + (i + 1) + " " + sectors[i].getClass().getName().split("[.]")[4]);
			} else {
				System.out.println("The sector" + (i + 1) + " was empty.");
			}
		}
	}

	public void totalArea() {
		double totalArea = 0;
		for (int i = 0; i < sectors.length; i++) {
			if (sectors[i] != null) {
				totalArea = totalArea + ((sectors[i]).getArea());
			}
		}
		System.out.println("Total area = " + totalArea);
	}
}
