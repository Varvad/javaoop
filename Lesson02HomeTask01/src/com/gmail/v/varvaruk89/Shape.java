package com.gmail.v.varvaruk89;

public abstract class Shape {

	public abstract double getPerimetr();

	public abstract double getArea();

}
