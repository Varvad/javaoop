package com.gmail.v.varvaruk89;

public class Triangle extends Shape {

	private Point a;
	private Point b;
	private Point c;

	public Triangle(Point a, Point b, Point c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public Triangle() {
		super();
	}

	public Point getA() {
		return a;
	}

	public void setA(Point a) {
		this.a = a;
	}

	public Point getB() {
		return b;
	}

	public void setB(Point b) {
		this.b = b;
	}

	public Point getC() {
		return c;
	}

	public void setC(Point c) {
		this.c = c;
	}

	@Override
	public double getPerimetr() {
		double perimetr;
		perimetr = a.getDist(b) + b.getDist(c) + c.getDist(a);
		return perimetr;
	}

	@Override
	public double getArea() {
		double area;
		double p;
		p = this.getPerimetr() / 2;
		area = Math.sqrt(p * (p - a.getDist(b)) * (p - b.getDist(c)) * (p - c.getDist(a)));
		return area;
	}

}
