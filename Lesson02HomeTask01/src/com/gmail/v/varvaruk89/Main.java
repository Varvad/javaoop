package com.gmail.v.varvaruk89;

public class Main {

	public static void main(String[] args) {
		Circle circle = new Circle(new Point(1, 2), new Point(3, 4));
		Triangle triangle = new Triangle(new Point(1, 2), new Point(3, 4), new Point(5, 6));
		Rectangle rectangle = new Rectangle(new Point(1, 2), new Point(3, 4), new Point(5, 6), new Point(7, 8));
		Board board = new Board();

		board.shapeInBoard(rectangle, 1);
		board.shapeInBoard(circle, 2);
		board.shapeInBoard(triangle, 3);
		board.shapeInBoard(rectangle, 4);
		board.outInformation();
		board.totalArea();
	}

}
