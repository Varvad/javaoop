package com.gmail.v.varvaruk89;

public class Circle extends Shape {
	private Point centre;
	private Point radius;

	public Circle(Point centre, Point radius) {
		super();
		this.centre = centre;
		this.radius = radius;
	}

	public Circle() {
		super();
	}

	public Point getCentre() {
		return centre;
	}

	public void setCentre(Point centre) {
		this.centre = centre;
	}

	public Point getRadius() {
		return radius;
	}

	public void setRadius(Point radius) {
		this.radius = radius;
	}

	@Override
	public double getPerimetr() {
		double perimetr;
		perimetr = 2 * Math.PI * centre.getDist(radius);
		return perimetr;
	}

	@Override
	public double getArea() {
		double area;
		area = Math.PI * centre.getDist(radius) * centre.getDist(radius);
		return area;
	}

}
