package com.gmail.v.varvaruk89;

import java.io.File;

public class MultyThreadCopyDir {
	static void copyDir(File fileFrom, File fileTo, int cntThread) {
		SingleThreadCopyDir[] threadarray = new SingleThreadCopyDir[cntThread];
		 
		for (int i = 0; i < threadarray.length; i++) {
			int size = fileFrom.listFiles().length / cntThread;
			int begin = size * i;
			int end = ((i + 1) * size);
			if ((fileFrom.listFiles().length - end) < size) {
				end =fileFrom.listFiles().length;
			}
			threadarray[i] = new SingleThreadCopyDir(fileFrom, fileTo, begin, end);

		}
	}

}