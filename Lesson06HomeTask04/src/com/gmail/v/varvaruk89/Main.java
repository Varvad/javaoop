package com.gmail.v.varvaruk89;

import java.io.File;

import javax.swing.JOptionPane;

public class Main {

	public static void main(String[] args) {

		File folderFrom ;
		File folderTo;
		String folderFromString;
		String folderToString;
		
		try {
			 folderFromString = String
					.valueOf(JOptionPane.showInputDialog("Input folder from what you want to copy files"));
			if (folderFromString.equals("")) {

				throw new NullPointerException();
				

			}else{
				 folderFrom = new File (folderFromString);
				
			}
			
			 folderToString = String
					.valueOf(JOptionPane.showInputDialog("Input folder in which you want to copy files"));
			if (folderToString.equals("")) {

				throw new NullPointerException();

			}else{
				folderTo = new File (folderToString);
			}
			
			int threadNumber = Integer.valueOf(JOptionPane.showInputDialog("Input number of thread"));
	
			MultyThreadCopyDir.copyDir(folderFrom, folderTo, threadNumber);
			
			
		} catch (NullPointerException e) {
			JOptionPane.showMessageDialog(null, "Parameter not entered,Null PointerException!");
		}catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Parameter not entered,Format Exception!");
			
		}
	
	}

}
