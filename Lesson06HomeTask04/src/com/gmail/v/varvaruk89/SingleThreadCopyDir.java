package com.gmail.v.varvaruk89;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class SingleThreadCopyDir implements Runnable{
	private File fileFrom; 
	private File fileTo;
	private int begin;
	private int end;
	private Thread thr;
	 

	public SingleThreadCopyDir(File fileFrom, File fileTo, int begin, int end) {
		super();
		
		this.fileFrom = fileFrom;
		this.fileTo = fileTo;
		this.begin = begin;
		this.end = end;
		thr = new Thread(this);
		thr.start();
	}

	@Override
	public void run(){
		
		if (!fileFrom.exists() | !fileTo.exists()) {
			throw new IllegalArgumentException("Wrong parametr! Directory not exists!");
		}

		File[] fileList = fileFrom.listFiles();
	
		for(int i=begin;i<end;i++)	
		{

			try (FileInputStream fis = new FileInputStream(fileList[i]);
					FileOutputStream fos = new FileOutputStream(fileTo + "/" + fileList[i].getName())) {
				byte[] bufferArray = new byte[4 * 1024];
				int readBytes = 0;
				for (; (readBytes = fis.read(bufferArray)) > 0;) {
					fos.write(bufferArray, 0, readBytes);
				}
				
				System.out.println(thr.getName()+"  copy "+fileList[i].getName());
				
			} catch (IOException e) {
				System.out.println(e);
			}

		}
		
	
	}
}