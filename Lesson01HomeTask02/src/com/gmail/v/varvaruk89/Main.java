package com.gmail.v.varvaruk89;

public class Main {

	public static void main(String[] args) {
		Triangle oneTriangle = new Triangle(35, 4, 5);
		oneTriangle.area();
		Triangle twoTriangle  = new Triangle(34,43,33);
		twoTriangle.area();
		Triangle threeTriangle = new Triangle(4,5,4);
		threeTriangle.area();
		System.out.println(oneTriangle.getArea());
		System.out.println(twoTriangle.getArea());
		System.out.println(threeTriangle.getArea());
	}

}
