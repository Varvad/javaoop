package com.gmail.v.varvaruk89;

public class Triangle {
	private double a;
	private double b;
	private double c;
	private double area;
	private boolean t = false;

	public Triangle(double a, double b, double c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public Triangle() {
		super();
	}

	public double getArea() {
		return area;
	}

	public double getA() {
		return a;
	}

	public void setA(double a) {
		this.a = a;
	}

	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}

	public double getC() {
		return c;
	}

	public void setC(double c) {
		this.c = c;
	}

	public void test() {
		if (a + b > c & a + c > b & b + c > a) {
			this.t = true;
		}
	}

	public void area() {
		test();
		if(this.t){
		double p = (a + b + c) / 2;
		this.area = Math.sqrt(p * (p - a) * (p - b) * (p - c));
		}else{
			//System.out.println("Triangle does not exist");
		}
	}
}
