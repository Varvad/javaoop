package com.gmail.v.varvaruk89;

public class BlackList {
	private Class blackList[] = new Class[0];
	private int counter = 0;
	private String name;

	public BlackList(String name) {
		super();
		this.name = name;
	}

	public BlackList() {
		super();

	}

	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCounter() {
		return counter;
	}

	public void add(Class<?> classOne) {

		if (!check(classOne)) {
			Class[] c = new Class[blackList.length + 1];
			System.arraycopy(blackList, 0, c, 0, blackList.length);
			c[c.length - 1] = classOne;
			counter++;
			blackList = c;
		} else {
			System.out.println("Class in BlackList");
		}
	}

	public boolean check(Class<?> classOne) {
		boolean test = false;
		if (counter > 0) {
			for (int i = 0; i < blackList.length; i++) {
				if (classOne == blackList[i]) {
					test = true;

				}
			}

		}

		return test;

	}

	public boolean  block (Object obj){
		boolean test = false;
		for (int i = 0;i<blackList.length;i++){
			if (blackList[i]==obj.getClass()){
				test = true;
			}
		}
		
		return test;
	}
	
	public void takeAway(Class<?> classOne) {

		if (counter == 0) {
			System.out.println("BlackList  is empty!");

		} else {
			counter = counter - 1;
			try {
				Class[] c = new Class[blackList.length - 1];
				System.arraycopy(blackList, 0, c, 0, blackList.length - 1);
				blackList = c;
			} catch (ArrayIndexOutOfBoundsException e) {

			}
		}

	}
}
