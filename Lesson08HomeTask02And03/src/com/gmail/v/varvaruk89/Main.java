package com.gmail.v.varvaruk89;

public class Main {
	public static void main(String[] args) {
		Cat cat = new Cat("Guga", 3);
		Cat catow = new Cat("Gugan", 3);
		Dog dog = new Dog("Boy", 4);
		Dog doger = new Dog("Boxer", 4);
		Human human = new Human("Vadim", 27);

		MySteck mySteck = new MySteck("wikipedia");

		mySteck.push(catow);
		mySteck.push(human);
		System.out.println(mySteck);

		BlackList drop = new BlackList("drop");
		drop.add(Cat.class);

		mySteck.pushAndTest(cat, drop);
		mySteck.pushAndTest(doger, drop);

		System.out.println(mySteck);

		drop.takeAway(Cat.class);
		mySteck.pushAndTest(cat, drop);
		System.out.println(mySteck);

	}
}