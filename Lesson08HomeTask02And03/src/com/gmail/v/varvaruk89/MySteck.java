
package com.gmail.v.varvaruk89;

public class MySteck {

	private String name;
	private Object[] mySteck = new Object[0];
	private int counter = 0;

	public MySteck(String name) {
		super();
		this.name = name;
	}

	public MySteck() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void push(Object obj) {
		Object[] c = new Object[mySteck.length + 1];
		System.arraycopy(mySteck, 0, c, 0, mySteck.length);
		c[c.length - 1] = obj;
		counter++;
		mySteck = c;

	}

	public void pushAndTest(Object obj, BlackList bl) {
		if (!checkList(obj, bl)) {
			Object[] c = new Object[mySteck.length + 1];
			System.arraycopy(mySteck, 0, c, 0, mySteck.length);
			c[c.length - 1] = obj;
			counter++;
			mySteck = c;

		} else {
			System.out.println("Objectes Class " + obj.getClass().getSimpleName() + " in blacklist.");
		}
	}

	public boolean checkList(Object obj, BlackList bl) {
		boolean test = false;
		if (bl.getCounter() > 0) {
			test = bl.block(obj);
		}
		return test;
	}

	public Object popShow() {
		Object obj = new Object();
		if (counter == 0) {
			System.out.println("Steck  is empty!");

		} else {
			obj = mySteck[mySteck.length - 1];

		}
		return obj;
	}

	public Object pop() {
		Object obj = new Object();
		if (counter == 0) {
			System.out.println("Steck  is empty!");

		} else {
			counter = counter - 1;
			try {
				obj = mySteck[mySteck.length - 1];
				Object[] c = new Object[mySteck.length - 1];
				System.arraycopy(mySteck, 0, c, 0, mySteck.length - 1);
				mySteck = c;
			} catch (ArrayIndexOutOfBoundsException e) {

			}
		}
		return obj;

	}

	@Override
	public String toString() {
		String mySteckString = "";

		if (counter == 0) {
			mySteckString = mySteckString + "Steck is empty";
		} else {
			System.out.println();
			System.out.println("Name mysteck " + name + " number of mysteck " + counter + ": ");
			for (int i = 0; i < this.mySteck.length; i++) {
				if (mySteck[i] != null) {
					mySteckString = mySteckString + " " + mySteck[i].toString() + System.lineSeparator();

				}
			}

		}

		return mySteckString;

	}
}